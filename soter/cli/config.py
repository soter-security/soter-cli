"""
Module containing CLI sub-commands for the Soter image method group.
"""

import itertools

import click

import yaml

from .util import soter_command, filter_scanners, report_result


@click.group()
def config():
    """
    Commands for interacting with Kubernetes resource manifests.
    """


def get_resources(filename):
    """
    Gets the resources from the given file.
    """
    # Read the file contents once
    if filename == "-":
        content = click.get_text_stream('stdin').read()
    else:
        with open(filename) as stream:
            content = stream.read()
    # Try to parse the content as multi-document YAML
    # Because JSON is a subset of YAML, this also works for JSON input
    # Also flatten any List resources, so output from kubectl can be piped in
    return itertools.chain.from_iterable(
        r['items'] if r['kind'] == 'List' else [r]
        for r in yaml.safe_load_all(content)
    )


@config.command()
@soter_command
@filter_scanners
@report_result
@click.argument(
    'files',
    nargs = -1,
    type = click.Path(dir_okay = False, readable = True, allow_dash = True)
)
async def scan(soter, files, scanners):
    """
    Scan the given resource manifests and display any issues.
    """
    resources = itertools.chain.from_iterable(get_resources(f) for f in files)
    kwargs = dict(resources = list(resources))
    if scanners:
        kwargs.update(scanners = scanners)
    return await soter.call("config.scan", **kwargs)
