"""
Module containing CLI sub-commands for the Soter image method group.
"""

import asyncio
import functools

import click

from .util import soter_command, filter_scanners, report_result


@click.group()
def image():
    """
    Commands for interacting with image vulnerabilities.
    """


@image.command()
@soter_command
@filter_scanners
@report_result
@click.option(
    '-f',
    '--force',
    is_flag = True,
    help = "If specified, force a re-scan without using the cache."
)
@click.argument('image')
async def scan(soter, image, scanners, force):
    """
    Scan the given image and display the vulnerability report.
    """
    kwargs = dict(image = image, force = force)
    if scanners:
        kwargs.update(scanners = scanners)
    return await soter.call("image.scan", **kwargs)
