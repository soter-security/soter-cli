"""
Module containing CLI sub-commands for the Soter info method group.
"""

import click

from .exceptions import NotFound
from .util import soter_command


@click.group()
def scanner():
    """
    Commands for viewing information about the configured scanners.
    """


@scanner.command(name = "list")
@soter_command
async def scanner_list(soter):
    """
    Display information about the configured scanners.
    """
    return await soter.call("info.scanners")


@scanner.command(name = "show")
@soter_command
@click.argument('name', metavar = 'SCANNER_NAME')
async def scanner_show(soter, name):
    """
    Display information about the named scanner.
    """
    try:
        return next(
            scanner
            for scanner in await soter.call("info.scanners")
            if scanner['name'] == name
        )
    except StopIteration:
        raise NotFound(f'No scanner with name "{name}".')
