"""
Module containing exceptions raised by the Soter CLI application.
"""

import json

import click


class SoterCliException(click.ClickException):
    """
    Base class for all Soter CLI exceptions.
    """


class EndpointConfigurationError(SoterCliException):
    """
    Raised when there is an error with the endpoint configuration.
    """


class NotFound(SoterCliException):
    """
    Raised when a specific named object is not found.
    """


class JsonRpcError(SoterCliException):
    """
    Raised when a JSON-RPC method call returns an error.
    """
    def __init__(self, error):
        super().__init__(error.message)
        self.error = error

    def show(self):
        # Print the error as JSON
        error_dict = dict(message = self.error.message, code = self.error.code, data = self.error.data)
        click.echo(json.dumps(error_dict, indent = 4))


class ExternalCommandFailed(SoterCliException):
    """
    Raised when a callout to an external command failed.
    """
