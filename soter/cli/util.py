"""
Module containing utilities for the Soter CLI.
"""

import asyncio
import enum
import functools
import json
import sys

import click

from jsonrpc.model import JsonRpcException

from jsonrpc.client import Client
from jsonrpc.client.transport.websocket import Transport

from .exceptions import EndpointConfigurationError, JsonRpcError


@functools.total_ordering
class Severity(enum.Enum):
    """
    Enum of possible severity levels for an issue.
    """
    NEGLIGIBLE = "NEGLIGIBLE"
    LOW = "LOW"
    MEDIUM = "MEDIUM"
    HIGH = "HIGH"
    UNKNOWN = "UNKNOWN"
    CRITICAL = "CRITICAL"

    def __lt__(self, other):
        # Assume that the severities are defined in order
        severities = list(self.__class__)
        return severities.index(self) < severities.index(other)


def soter_command(func):
    """
    Decorator that turns the wrapped function into a Soter CLI command.

    Soter CLI commands are async functions that recieve a configured Soter JSON-RPC
    client as the named argument ``soter``. Any data they return is output in JSON
    format. Optionally, the return value can be a ``(data, exit_code)`` tuple if a
    non-zero exit code is required.
    """
    # Process the incoming arguments, run the wrapped function and format the output
    async def run(endpoint, *args, **kwargs):
        if not endpoint:
            raise EndpointConfigurationError('No endpoint specified.')
        # Configure the client and call the command
        # Catch any problems executing JSON-RPC methods and display them nicely
        try:
            async with Client(Transport(endpoint)) as client:
                result = await func(*args, soter = client, **kwargs)
        except JsonRpcException as exc:
            raise JsonRpcError(exc)
        # If the result is a tuple of length 2 where the second element is an int,
        # treat it as a (data, exit_code) tuple
        if isinstance(result, tuple) and len(result) == 2 and isinstance(result[1], int):
            data, exit_code = result
        else:
            data, exit_code = result, 0
        # Pretty-print the data as JSON then exit with the given exit code
        click.echo(json.dumps(data, indent = 4))
        sys.exit(exit_code)

    # The actual command function is a sync version of the async function above
    @functools.wraps(func)
    def command(*args, **kwargs):
        return asyncio.run(run(*args, **kwargs))

    # Add click options for configuring the Soter endpoint
    command = click.option(
        '--endpoint',
        help = 'URL of the Soter API endpoint.',
        metavar = 'URL',
        envvar = 'SOTER_ENDPOINT'
    )(command)

    return command


#: Decorator for commands that allows the used scanners to be filtered.
filter_scanners = click.option(
    '-s',
    '--scanner',
    'scanners',
    multiple = True,
    help = 'Limit results to the specified scanner. Can be specified multiple times.',
    metavar = 'SCANNER'
)


def report_result(func):
    """
    Decorator for commands that produce a report containing a list of issues.

    Ensures that the exit status is set correctly based on a configurable threshold.
    """
    @functools.wraps(func)
    async def command(threshold, *args, **kwargs):
        # Call the wrapped command to get the report
        report = await func(*args, **kwargs)
        # If there is an issue with a severity over the threshold, exit with a non-zero code
        # If there are no issues, exit with zero exit code
        if report['issues']:
            # Note that issues are sorted by severity from highest to lowest, so checking
            # the severity of the first issue is sufficient
            failed = Severity[report['issues'][0]['severity'].upper()] >= Severity[threshold.upper()]
            return report, 1 if failed else 0
        else:
            return report

    # Add a configurable threshold option
    command = click.option(
        '-t',
        '--threshold',
        type = click.Choice([s.name.lower() for s in Severity], case_sensitive = False),
        default = "high",
        help = 'The threshold at which to return a non-zero exit status.',
        show_default = True
    )(command)

    return command
