"""
Main module for the soter CLI.
"""

import click

from .info import scanner
from .image import image
from .config import config
from .namespace import namespace


@click.group()
def cli():
    """
    Command line interface for the Soter security scanning tool.
    """


# Add the subcommands to the main command
cli.add_command(scanner)
cli.add_command(image)
cli.add_command(config)
cli.add_command(namespace)
