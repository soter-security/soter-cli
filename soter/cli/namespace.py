"""
Module containing CLI sub-commands for the Soter namespace method group.
"""

import pathlib
import shlex
import subprocess

import click

from .util import soter_command, filter_scanners, report_result
from .exceptions import ExternalCommandFailed


@click.group()
def namespace():
    """
    Commands for interacting with pod security issues.
    """


@namespace.command()
@soter_command
@report_result
@click.option(
    '--kinds',
    help = "Comma-separated list of kinds to scan. "
           "If not specified, the default kinds will be used."
)
@click.option(
    '-A',
    '--all-namespaces',
    is_flag = True,
    help = "If specified, scan all namespaces for the specified context."
)
@filter_scanners
@click.option(
    '--kubeconfig',
    envvar = 'KUBECONFIG',
    type = click.File('r'),
    help = "Path to the kubeconfig file to use to connect to Kubernetes."
)
@click.option(
    '--kubeconfig-cmd',
    help = "Command to use to generate a kubeconfig file if none is specified.",
    metavar = 'COMMAND',
    envvar = 'SOTER_KUBECONFIG_CMD'
)
@click.argument('namespace', required = False)
async def scan(soter, kinds,
                      namespace,
                      all_namespaces,
                      kubeconfig,
                      kubeconfig_cmd,
                      scanners):
    """
    Scan the given namespace and display a vulnerability report.
    """
    # Get the kubeconfig content to submit
    if kubeconfig is not None:
        # If kubeconfig is specified, use that
        kubeconfig_content = kubeconfig.read()
    elif kubeconfig_cmd is not None:
        # If kubeconfig is not specified but kubeconfig-cmd is, use the stdout from executing it
        result = subprocess.run(shlex.split(kubeconfig_cmd), stdout = subprocess.PIPE)
        if result.returncode != 0:
            raise ExternalCommandFailed('kubectl command failed - aborting.')
        kubeconfig_content = result.stdout.decode()
    else:
        # If neither kubeconfig or kubeconfig-cmd are specified, try to use the default location
        # If it doesn't exist, there is nothing more we can try so just let the error bubble
        kubeconfig = pathlib.Path.home() / '.kube' / 'config'
        with kubeconfig.open() as fh:
            kubeconfig_content = fh.read()
    # For the auth, use the kubeconfig kind with the discovered content
    kwargs = dict(auth = dict(kind = "kubeconfig", kubeconfig = kubeconfig_content))
    # all-namespaces takes precedence over a specific namespace
    if all_namespaces:
        kwargs.update(all_namespaces = True)
    if namespace:
        kwargs.update(namespace = namespace)
    if kinds:
        kwargs.update(kinds = [k.strip() for k in kinds.split(',')])
    if scanners:
        kwargs.update(scanners = scanners)
    # Only add variables that are specified to the args
    return await soter.call("namespace.scan", **kwargs)
