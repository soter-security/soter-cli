#!/usr/bin/env python3

import os, re
from setuptools import setup, find_namespace_packages


here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.md')) as f:
    README = f.read()


if __name__ == "__main__":
    setup(
        name = 'soter-cli',
        setup_requires = ['setuptools_scm'],
        use_scm_version = True,
        description = 'Command line interface (CLI) for the Soter scanner.',
        long_description = README,
        classifiers = [
            "Programming Language :: Python",
        ],
        author = 'Matt Pryor',
        author_email = 'matt.pryor@stfc.ac.uk',
        url = 'https://github.com/mkjpryor-stfc/soter-cli',
        packages = find_namespace_packages(include = ['soter.*']),
        include_package_data = True,
        install_requires  = [
            'click',
            'jsonrpc-asyncio-client[websockets]',
        ],
        entry_points = {
            'console_scripts': ['soter=soter.cli.main:cli']
        }
    )
